#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "Food.generated.h"

UCLASS()
class SNAKEGAME1_API AFood : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:	
	AFood();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void interact(AActor* interactor, bool bIsHead) override;
};