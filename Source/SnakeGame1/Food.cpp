#include "Food.h"
#include "SnakeBase.h"
#include <math.h>
using namespace std;

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();	
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
				
			//int RandomX = FMath::RandRange (Min : -848, Max : 448);
			//int RandomY = FMath::RandRange (Min : -1454, Max : 1454);
			//this->SetActorLocation(FVector(RandomX, RandomY, inZ:0));			
		}
	}
}