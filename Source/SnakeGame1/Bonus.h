#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "Bonus.generated.h"

UCLASS()
class SNAKEGAME1_API ABonus : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:
	ABonus();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void interact(AActor* interactor, bool bIsHead) override;
};