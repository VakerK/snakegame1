#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Food.h"
#include "Bonus.h"
 
APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();	
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BuferTime += DeltaTime;
	if (BuferTime > StepDeley)
	{
		AddRandomFood();
		BuferTime = 0;
	}	
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::AddRandomFood()
{
	FRotator StartPositionRatation = FRotator(0, 0, 0);

	float SpawnY = FMath::FRandRange(MinY, MaxY);
	float SpawnX = FMath::FRandRange(MinX, MaxX);

	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	if (SnakeActor)
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<AFood>(StartPoint, StartPositionRatation);
		}
	}
}

void APlayerPawnBase::AddRandomBonus()
{
	FRotator StartPositionRatation = FRotator(0, 0, 0);

	float SpawnY = FMath::FRandRange(MinY, MaxY);
	float SpawnX = FMath::FRandRange(MinX, MaxX);

	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	if (SnakeActor)
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<ABonus>(StartPoint, StartPositionRatation);
		}
	}
}