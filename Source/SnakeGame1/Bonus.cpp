#include "Bonus.h"
#include "SnakeBase.h"

ABonus::ABonus()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABonus::BeginPlay()
{
	Super::BeginPlay();	
}

void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABonus::interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->MovementSpeed = Snake->MovementSpeed * 0.75f;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			Destroy();
		}
	}
}