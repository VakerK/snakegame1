#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interacteble.generated.h"

UINTERFACE(MinimalAPI)
class UInteracteble : public UInterface
{
	GENERATED_BODY()
};

class SNAKEGAME1_API IInteracteble
{
	GENERATED_BODY()

public:
	virtual void interact(AActor* Interactor, bool bIsHead);
};