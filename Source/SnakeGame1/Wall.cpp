#include "Wall.h"
#include "SnakeBase.h"

AWall::AWall()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AWall::BeginPlay()
{
	Super::BeginPlay();	
}

void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWall::interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}