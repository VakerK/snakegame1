#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME1_API ASnakeElementBase : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:	
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY()
	ASnakeBase* SnakeOwner;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BluePrintNativeEvent)
	void SetFirstElementType();

	virtual void interact(AActor* interactor, bool bIsHead) override;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
						  AActor* OtherActor, 
						  UPrimitiveComponent* OtherComp, 
						  int32 OtherBodeIndex, 
						  bool bFromSweep, 
						  const FHitResult& SweepResult);
	UFUNCTION()
	void ToggleCollision();
};